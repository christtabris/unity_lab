﻿using UnityEngine;
using System.Collections;

public class Turbol : MonoBehaviour {
	[SerializeField]
	OnTriggerDirectionalForce force;
	[SerializeField]
	bool isApplyForceForever=false;
	bool isApplyForce=false;
	public float forceSpeed;
	[SerializeField]
	SpringPlat sp;
	//Renderer[] rs;
	// Use this for initialization
	void Start () {
		//rs=GetComponentsInChildren<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isApplyForce||isApplyForceForever)
			force.ForceSpeed=forceSpeed;
		else
			force.ForceSpeed=0;
//		foreach(Renderer r in rs){
//			Color cw=r.material.color;
//			if(forceSpeed>0)
//				cw.a=1;
//			else
//				cw.a=.2f;
//			r.material.color=cw;
//		}
	}

	public void fire(){
		StartCoroutine(WaitAndPrint());
	}

	IEnumerator WaitAndPrint() {
		sp.play();
		isApplyForce = true;
		yield return new WaitForSeconds(.5f);
		isApplyForce = false;
	}

}
