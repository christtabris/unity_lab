﻿using UnityEngine;
using System.Collections;

public class PickableIni : MonoBehaviour {

	Vector3 iniP;
	Quaternion iniR;
	Vector3 iniS;

	// Use this for initialization
	void Start () {
		iniP = transform.position;
		iniR = transform.rotation;
		iniS = transform.lossyScale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision) {
	
		if (collision.collider.gameObject.layer == 20) {
			StartCoroutine(WaitAndPrint());
		}
	}

	IEnumerator WaitAndPrint() {
		GameObject spPrefab = GameObject.Find ("SceneLogic/stages/Spark");
		GameObject saPrefab = GameObject.Find ("SceneLogic/stages/skillAttack");

		GameObject fake = GameObject.Instantiate (gameObject,transform.position,transform.rotation) as GameObject;

		GameObject.Destroy (fake.collider);
		fake.rigidbody.useGravity = false;

		GameObject sp = GameObject.Instantiate (spPrefab) as GameObject;
		sp.transform.parent = fake.transform;
		sp.transform.localPosition = Vector3.zero;

		//GameObject.Destroy (fake.rigidbody);
		TweenScale.Begin (fake, 0.85f, Vector3.zero);
		//TweenAlpha.Begin (fake, 0.85f, 0);
		transform.position = new Vector3 (50000,0,0);
		TweenScale.Begin (gameObject, 0f, Vector3.zero);
		//transform.lossyScale = Vector3.zero;

		yield return new WaitForSeconds(0.85f);
		GameObject.Destroy (fake);

		yield return new WaitForSeconds(0.15f);
		transform.position = iniP;
		transform.rotation = iniR;
		TweenScale.Begin (gameObject, 0.2f, iniS);
		GameObject sa = GameObject.Instantiate (saPrefab,iniP,iniR) as GameObject;
		sa.particleSystem.Play ();
		yield return new WaitForSeconds(0.5f);
		GameObject.Destroy (sa);
	}
}
