﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerBeheaver : InputListener {
	
	public enum ControlType{mono,stereo };
	public ControlType controlType;
	[SerializeField]
	Transform corsor,hand, crosshair;
	SelectController select;
	IKController ikControl;
	GameObject detected_object;
	KinectModelControllerV2 kinectModelController;
	GrapDetector grapDetector;
	PlayerAnimation playerAni;
	bool isT=false;
	Vector3 initialPos;
	[SerializeField]
	Cardboard cb;
	[SerializeField]
	CardboardHead cbh;
	MonoController monoInput;
	WiiController wiiInput;

	public string playerId;

	void Awake() {
		DateTime now=DateTime.UtcNow;
		playerId="subject-"+now.ToString("yyMMddHHmm");
	}
	
	void Start () {

		initialPos=transform.position;
		grapDetector = GetComponent<GrapDetector> ();
		ikControl=GetComponent<IKController>();
		grapDetector.onObjectEnter+=onObjectDetectEnter;
		grapDetector.onObjectLeave+=onObjectDetectLeave;
		select=GameObject.Find("SceneLogic").GetComponent<SelectController>();
		kinectModelController = GetComponent<KinectModelControllerV2> ();
		playerAni=GetComponent<PlayerAnimation>();
		bool isKinectInited= KinectSensor.IsInitialized;
		
		monoInput=GetComponent<MonoController>();
		wiiInput=GetComponent<WiiController>();
		
		if(controlType==ControlType.mono){
			monoInput.enabled=true;
			wiiInput.enabled=false;
			grapDetector.target = corsor;
			//qr.gameObject.SetActive(false);
		}if(controlType==ControlType.stereo){
			grapDetector.target = hand;
			if(GameObject.Find ("SceneLogic").GetComponent<ClienTest>().isServer){
			rigidbody.isKinematic=true;
				collider.enabled=false;
				cb.enabled=false;
				cbh.enabled=false;
			}
		}
		
	}
	
	// Update is called once per frame
	void Update () {

		//base.Update();
		playerAni.direction=Horizontal;
		playerAni.speed=Vertical;

		if(detected_object!=null&&GetButtonDown("Fire3"))
			select.onSelect(detected_object);
		if (controlType == ControlType.stereo) {
			crosshair.position = grapDetector.end.position;	
			crosshair.rotation=grapDetector.end.rotation;
		} else {
			Camera uicamera=Camera.main;
			Vector3 p= new Vector3(Screen.width/2,
			                       Screen.height/2, uicamera.nearClipPlane+.1f);
			crosshair.position = uicamera.ScreenToWorldPoint(p);
			//qr.localScale=Vector3.zero;
			crosshair.localScale=new Vector3(.13f,.13f,1);
		}
	}
	
	IEnumerator  reset(float s){
		yield return new WaitForSeconds(s);
		rigidbody.isKinematic = false;
		rigidbody.detectCollisions = true;
		transform.position=initialPos;
		playerAni.Enable();
	}
	
	void onObjectDetectEnter(GameObject obj){
		detected_object=obj;
	}
	
	void onObjectDetectLeave(GameObject obj){
		detected_object=null;
	}
	
	public string GrapObjectName{
		get{
			if(detected_object==null)
				return null;
			else
				return detected_object.name;
		}
	}

	public void onConnected(){
//		qr.gameObject.SetActive(false);
	}

	public void DoDamage(float d){
		if (d > 0) {
			isT = true;
		}
	}


	float v = 0;
	float h = 0;
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		//if(GameObject.Find ("SceneLogic").GetComponent<ClienTest>().isServer)
		//	return;
		if(controlType==ControlType.mono)
			return;
		if (stream.isWriting) 
		{//send
			h = Horizontal;
			stream.Serialize(ref h);
			v = Vertical;
			stream.Serialize(ref v);
		} else {//receive
			stream.Serialize(ref h);
			Horizontal = h;
			stream.Serialize(ref v);
			Vertical = v;
		}
	}

	public override void SetButtonValue (string b, bool p)
	{
		if(controlType==ControlType.stereo&& GameObject.Find ("SceneLogic").GetComponent<ClienTest>().isServer&&
		   Network.connections.Length>0)
			networkView.RPC("SetButtonValueRPC",RPCMode.OthersBuffered,b,p);
		else
			base.SetButtonValue (b, p);
	}

	void OnConnectedToServer() {
		#if UNITY_ANDROID
		Debug.Log("Connected to server");
		networkView.RPC("playerloginRPC",RPCMode.OthersBuffered,playerId);
		#endif
	}

	[RPC]
	void SetButtonValueRPC(string b, bool p) {
		base.SetButtonValue (b, p);
	}

	[RPC]
	void playerloginRPC(string id) {
		playerId = id;
		Debug.Log(id+" connected");
	}

		
}
