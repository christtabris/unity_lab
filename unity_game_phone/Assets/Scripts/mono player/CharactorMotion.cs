﻿using UnityEngine;
using System.Collections;

public class CharactorMotion : MonoBehaviour {

	[SerializeField]
	Transform target;
	[SerializeField]
	Transform cameraAvator;
	[SerializeField]
	float speed=2.5f;
	void Start () {
		target.parent=transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
		PlayerBeheaver pb=GetComponent<PlayerBeheaver>();

		target.position=cameraAvator.position;
		InputListener inp=GetComponent<InputListener>();
		Vector3 direction=new Vector3(inp.Horizontal,0,inp.Vertical);
		if(!(pb.controlType==PlayerBeheaver.ControlType.stereo&&
		     GameObject.Find ("SceneLogic").GetComponent<ClienTest>().isServer)){
			transform.Translate(direction*speed * Time.deltaTime, Space.Self);
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 
			                                         target.localEulerAngles.y, 
			                                         transform.localEulerAngles.z); 
		}

//		#if UNITY_ANDROID
//		if(!GameObject.Find ("SceneLogic").GetComponent<ClienTest>().isServer)
//		networkView.RPC("SetposrotRPC",RPCMode.OthersBuffered,transform.position,target.rotation);
//		#endif

	}

	void LateUpdate() {
		#if UNITY_ANDROID
		if(!GameObject.Find ("SceneLogic").GetComponent<ClienTest>().isServer)
			networkView.RPC("SetposrotRPC",RPCMode.OthersBuffered,transform.position,target.rotation);
		#endif
	}

	[RPC]
	void SetposrotRPC(Vector3 pos, Quaternion rot) {
		#if UNITY_EDITOR
		target.rotation=rot;
		transform.position=pos;
		transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 
		                                         target.localEulerAngles.y, 
		                                         transform.localEulerAngles.z); 
		#endif
	}

}
